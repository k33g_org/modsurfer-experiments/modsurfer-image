#!/bin/bash
set -o allexport; source .env; set +o allexport

#WASM_URI="/api/v4/projects/${GITLAB_WASM_PROJECT_ID}/packages/generic/${WASM_PACKAGE}/${WASM_VERSION}/${WASM_MODULE}"
#curl https://${WASM_REGISTRY}${WASM_URI} --output ${WASM_MODULE}

#docker run -it ${IMAGE_NAME} sh

#docker run ${IMAGE_NAME} ./modsurfer --version
#docker -v $(pwd)/:/cli run ${IMAGE_NAME} "./modsurfer validate -p ${WASM_MODULE} -c ${WASM_MODULE}.yaml"

docker run \
  --volume "$PWD":/cli \
  ${IMAGE_NAME} ./modsurfer validate -p ${WASM_MODULE} -c ${WASM_MODULE}.yaml
